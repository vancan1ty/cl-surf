;; intended to be loaded automatically by daemontools
(load "surf.asd")
(load "berryutils.lisp")

(asdf:load-system :surf)

(defpackage startsurf
  (:use :common-lisp :com.vancan1ty.util))

(in-package :startsurf)

(defun pprint-plist (plist)
  (format t "~{~a:~10t~a~%~}~%" plist))

(defparameter *last-update-timestamp* 0)
(defparameter *seconds-between-updates* (* 60 60 4));once every four hours

(defun index-count(settings-obj)
  (length
            (uiop:directory-files (uiop:ensure-directory-pathname (getf settings-obj :directory)))))


(defun reindex ()
  (format t "reindexing ~a~%" (getf controller::*currentsite* :siteroot))
  (com.vancan1ty.controller::setup-search-wrapper)
  (setq *last-update-timestamp* (get-universal-time))
  (format t "there are ~a files in ~a~%" (index-count controller::*currentsite*) (getf controller::*currentsite* :directory)))

(defun runsurf-reindexperiodically ()
  (let* ((currentsettings controller::*currentsite*)
         (numinindex (index-count currentsettings)))
    (format t "Here are your current settings~%")
    (pprint-plist currentsettings)
    (format t "there are ~a files in ~a~%" numinindex (getf currentsettings :directory))
    (do () (())
      (if (< (- (get-universal-time) *last-update-timestamp*)
                *seconds-between-updates*)
          (progn (format t "sleeping~%")
                 (sleep 600))
        (reindex)))))

;;AUTOMATICALLY START THINGS UP...
(runsurf-reindexperiodically)
