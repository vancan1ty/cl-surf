;;; the purpose of this file is to define the configuration/the parameter *currentsite*

(in-package :com.vancan1ty.controller)

;; sample
(defparameter vancan1tysite
  (list :siteroot "https://vancan1ty.com"
        :sitename "vancan1ty.com"
        :ui-root-url "/search"
        :stayonsite t
        :depth 8
        :directory "index_vancan1ty"
        :directories-to-avoid '()
        :url-regexes-to-suppress '("^https://vancan1ty.com/blog$"
                                   "^https://vancan1ty.com/blog/page/[0-9]*$"
                                   "https://vancan1ty.com/blog/chronological-posts"
                                   "https://vancan1ty.com/blog/postsForTag"
                                   "https://vancan1ty.com/blog/posts-for-tag"
                                   )
        ))

(defparameter *currentsite* vancan1tysite)

;; some additional sample configurations
;; (defparameter sossite
;;   (list :siteroot "http://www.sosmath.com"
;; 	:stayonsite t
;; 	:depth 4
;; 	:directory "index_sosmath/"
;; 	:directories-to-avoid '("http://www.sosmath.com/CBB" "http://www.sosmath.com/memberlist")
;; ))
;;
;; (defparameter clikisite
;;   (list :siteroot "http://www.cliki.net"
;; 	:stayonsite t
;; 	:depth 6
;; 	:directory "index_cliki/"
;; 	:directories-to-avoid '("http://www.cliki.net/site")))
;;
;; (defparameter dailywtfsite
;;   (list :siteroot "http://www.thedailywtf.com"
;;  	:stayonsite t
;;  	:depth 4
;;  	:directory "thedailywtfindex/"
;;  	:directories-to-avoid (make-directories-to-avoid-list "http://www.thedailywtf.com" '(
;; 											     "/Resources/"
;; 											     "/Admin/"
;; 											     "/Comments/"))))
;;
;; (defparameter greystonesite
;;   (list :siteroot "http://www.campgreystone.com"
;; 	:stayonsite t
;; 	:depth 5
;; 	:directory "index_greystone/"
;; 	:directories-to-avoid '("http://www.campgreystone.com/live" "http://www.campgreystone.com/news/photos")))
