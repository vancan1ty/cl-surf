(defpackage :com.vancan1ty.controller
  (:nicknames :controller)
  (:use :common-lisp :alexandria :com.vancan1ty.util)
  (:import-from :com.vancan1ty.wordstat :bootstrap-image :*tothash* :*totnum*)
  (:import-from :com.vancan1ty.crawler :index-sites-wrapper :create-standard-index-site-p)
  (:import-from :uiop :ensure-pathname :ensure-directory-pathname :merge-pathnames)
  (:import-from :com.vancan1ty.mem-cache-handler :init-memcache :update-memcache)
  (:export
   :setup-search
   :run-search
   :*tinfo*
   :*currentsite*
   :init-totnum-tothash
   :site-index-info-visited-hash
   :site-index-info-memcache
   :site-index-info-directory
   *currentsite*
   :compute-url-relative-to-root
   ))

(in-package :com.vancan1ty.controller)

(defun make-directories-to-avoid-list (root dirs)
  (loop for dir in dirs collect
       (concatenate 'string root dir)))

(defstruct site-index-info
  (visited-hash ())
  (memcache ())
  (directory ()))

;;CBNOTE this value must be configured in config.lisp
(defparameter *currentsite* nil)

(load "config.lisp")

(defvar *tinfo* ())
(defvar *totnum* ())
(defvar *tothash* ())

(defun compute-url-relative-to-root (relativepath)
                   (let* ((ui-root (getf *currentsite* :ui-root-url))
                          (relative-path-no-leading-slash (berryutils:remove-prefix relativepath "/")))
                     (berryutils:print-transparent (berryutils:remove-suffix (concatenate 'string ui-root "/" relative-path-no-leading-slash) "/"))))

(defun setup-search-wrapper ()
  (setf *tinfo* (setup-search
                 (getf *currentsite* :siteroot)
                 (getf *currentsite* :directory)
                 (getf *currentsite* :stayonsite)
                 (getf *currentsite* :depth)
                 (getf *currentsite* :directories-to-avoid)
                 (getf *currentsite* :url-regexes-to-suppress))))

(defun setup-search (baseurl directory stayonsite depth &optional (directories-to-avoid nil) (url-regexes-to-suppress nil))
  (format t "hello 1 ~%")
  (if (not (boundp '*tothash*))
      (com.vancan1ty.wordstat:bootstrap-image))
  (let* ((mvisited-hash
           (crawler:index-sites-wrapper
            (list baseurl)
            depth
            directory
            (crawler:create-standard-index-site-p 100000000)
            :stay-on-sites stayonsite
            :directories-to-avoid directories-to-avoid
            :url-regexes-to-suppress url-regexes-to-suppress))
         (mmemcache (mchandler:init-memcache (ensure-directory-pathname directory))))
    (make-site-index-info :visited-hash mvisited-hash :memcache mmemcache :directory directory)))

(defun restore-search ()
  (let ((visitedhash
          (alist-hash-table
           (loop for file in
                          (uiop:directory-files (elt (directory (ensure-directory-pathname (getf *currentsite* :directory))) 0))
                 collect
                 (let ((filecontents (with-open-file (stream file) (with-standard-io-syntax (read stream)))))
                   (cons (getf filecontents :url) (getf filecontents :timeindexed))))))
        (memcache (mchandler:init-memcache (ensure-directory-pathname (getf *currentsite* :directory))))
        (directory (getf *currentsite* :directory)))
    (setf *tinfo* (make-site-index-info :visited-hash visitedhash :memcache memcache :directory directory))))

(defun index-single-web-page-wrapper (url)
  (crawler:index-single-web-page (site-index-info-memcache *tinfo*) url (site-index-info-visited-hash *tinfo*) (site-index-info-directory *tinfo*)))

(defun run-search (siteindexinfo querytext start end)
  (with-slots (visited-hash memcache directory) siteindexinfo
    (searcher:run-search memcache directory querytext start end)))

(defun init-totnum-tothash ()
  (let ((totdata (with-open-file (stream "totdata.lisp") (read stream))))
    (setf *totnum* (getf totdata :totnum))
    (setf *tothash* (alist-hash-table (getf totdata :tothash))))
  ())
