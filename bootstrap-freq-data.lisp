;;This code initializes the word frequency data from the brown corpus when loaded...
(defpackage :com.vancan1ty.bootstrap-freq-data
  (:nicknames :bootstrap-freq-data)
  (:use :common-lisp :com.vancan1ty.util :com.vancan1ty.wordstat :com.vancan1ty.controller)
  )

(in-package :com.vancan1ty.bootstrap-freq-data)

(defvar *bootstrap-complete* nil)

(init-totnum-tothash )
(setf *bootstrap-complete* t)
