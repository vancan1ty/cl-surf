# CL-SURF#
## Very easy self-hosted site search #
Author: Currell Berry
License Apache License 2.0 + please attribute my work.

My goal with Cl-Surf is to make it trivially easy to install simple site search on an Apache or Hunchentoot hosted site.  It's completely functional for smaller sites at the moment, but needs tweaks to the way it retrieves cached information from disk and processes terms (need to add stemming) before it will be quality persay.

## Installation Steps
1. Download
```bash
    git clone https://gitlab.com/vancan1ty/cl-surf
```

2. Configure
Modify config.lisp to point to your website.

`*currentsite*` must be configured to point to a valid configuration similar to the example vancan1tysite configuration that is given.

3. Run
```bash
    $ sbcl --load 'startsurf.lisp'
```

4. You will be presented with a message like the following

```
    Here are your current settings
    SITEROOT: https://vancan1ty.com
    SITENAME: vancan1ty.com
    STAYONSITE: T
    DEPTH:    8
    DIRECTORY: index_vancan1ty
    DIRECTORIES-TO-AVOID: NIL
    URL-REGEXES-TO-SUPPRESS: (^https://vancan1ty.com/blog$
                              ^https://vancan1ty.com/blog/page/[0-9]*$
                              https://vancan1ty.com/blog/chronological-posts
                              https://vancan1ty.com/blog/postsForTag
                              https://vancan1ty.com/blog/posts-for-tag)

    there are 63 files in index_vancan1ty
    What would you like to do?

                 (0) restore from disk

                 (1) index this site again

                 (2) index a different site
```

If this is your first time running, you will want to enter 1 to request indexing of the website you want to crawl/setup search for.
The crawler will run for a time. When it is done, the search engine will become available at http://localhost:8081

Otherwise, you may want to enter 0 at the prompt to restore the results from the last crawl.

5. Set it up to run automatically (if you want to run this on a server)
The startsurf_fromservice.lisp script starts up cl_surf and periodically reindexes the configured site.  Adjust it to your needs for your specific usecase.  Then follow the below steps to configure cl-surf to run as a service (assuming your system has systemd on it -- you must adapt if you use another init system).

    1. Create a user for your service to run under -- recommend the username cl_surf
    2. Copy the cl-surf project into the home folder of your new user on your server
    3. Verify that you can run cl-surf using "sbcl --load 'startsurf.lisp'" interactively on your server (when your active user is cl_surf)
    4. Copy the systemd unit file cl-surf.service to /etc/systemd/system (or equivalent folder on your system)
    5. Run `sudo systemctl daemon-reload` to get systemd to see your new daemon
    6. Run `sudo systemctl enable cl-surf.service` to enable cl-surf in systemd
    7. Run `sudo systemctl start cl-surf.service` to actually start cl-surf in systemd
    8. I recommend checking `journalctl -xeu cl-surf.service` to see what the cl-surf logs have to say to verify everything started up ok
    9. At the end of this process the cl-surf service should be up and running on your server, you should be able to `curl localhost:8081` to verify that it's up and running.  You may need to set up a reverse proxy or firewall config to make this service accessible to the external world, depending on your system's specific configuration.
