;;;; author: Currell Berry
;;
;;;; You may use or modify this program for any purpose, but please
;;;; include my name in the source.

(defpackage :surf-web-interface
  (:use :common-lisp :hunchentoot :cl-who :com.vancan1ty.controller :com.vancan1ty.util))
(in-package :surf-web-interface)

;;replace this with your installation directory...
(defvar *install-dir* (concatenate 'string (namestring *default-pathname-defaults*) "/web/"))

(setf *dispatch-table*
      (list #'dispatch-easy-handlers))
(setf *show-lisp-errors-p* t
      *show-lisp-backtraces-p* t)

;;(setf hunchentoot:*catch-errors-p* nil)

;; (define-easy-handler (easy-demop :uri "/sitesearch" :default-request-type :both)
;;     ((searchquery :request-type :post ))
;;   ;;set numinputs and truthtableinput to correct values for post and get scenarios
;;   (let* ((searchtext (if searchquery searchquery))

;;     (output (controller:run-search *tinfo* searchtext 0 10)))
;;     (log-message* :INFO "**********post input:~%searchquery: ~a~%" searchquery)

;;     ;(log-message* :INFO "doing post! output: ~a " output)
;;     output))

(getf controller:*currentsite* :sitename)

;;(run-search controller:*tinfo* "python" 0 10)

(defun run-search-generate-page (query start end)
  (format t "run-search-generate-page ~a ~a ~a~%" query start end)
  (let* ((results-struct (run-search controller:*tinfo* query start end))
         (sitename (getf controller:*currentsite* :sitename))
         (siteroot (getf controller:*currentsite* :siteroot))
         (totalresultcount (com.vancan1ty.searcher::results-and-meta-totalresultcount results-struct))
         (query-millis (com.vancan1ty.searcher::results-and-meta-querytimemillis results-struct))
         (query-millis-phrase (if (> query-millis 0)
                                  (format nil "query took ~a milliseconds" (round query-millis))
                                  ""))
         (hasmore (> totalresultcount end))
         (end-to-display (min totalresultcount end))
         (stylepage-url (controller:compute-url-relative-to-root "/styles.css")))
    (format t "results-struct ~a~%" results-struct)
    (with-html-output-to-string (*standard-output* nil :prologue t :indent t)
      (:html
       (:head (:title "cl-surf kweb search")
              (:link :href stylepage-url :type "text/css" :rel "stylesheet" nil))
       (:body
        (:div :id "wrapper"
              (:div :id "toparea"
                    (:div :style
                          "top: -5px; position: relative; margin-left: auto; margin-right: auto; text-align: center"
                          (:span "searching"
                                 (:a :href
                                     siteroot
                                     (write-string sitename))))
                  (:h1
                    (:a :href "https://gitlab.com/vancan1ty/cl-surf" (write-string "cl-surf")))
                    (:p (:form
                         :method :get
                         (:input :type :text
                                 :id "querybox"
                                 :name "query"
                                 :value query)
                         (:input :id "submitbutton" :type :submit :value "Search")))
                    (:div :style "clear: both"))
              ;;(:div :id "infoline"
              ;;      (:p (fmt "searching ~a..." sitename)))
              (:div :id "resultsarea"
                    (:p (if (com.vancan1ty.util:string-is-nonempty query)
                            (com.vancan1ty.searcher:format-search-results-as-html (com.vancan1ty.searcher::results-and-meta-results results-struct))
                            )))
              (if (and query
                       (> (length query) 0))
                  (htm (:div :id "bottomcontrols"
                             (if (>= start 10)
                                 (htm (:form :action "/" :method "GET"
                                             (:input :type "submit" :class "navbutton" :value "Prev")
                                             (:input :type "hidden" :name "query" :value query)
                                             (:input :type "hidden" :name "start" :type "hidden" :value (- start 10))
                                             (:input :type "hidden" :name "end" :type "hidden" :value start))))

                             (if hasmore
                                 (htm
                                  (:form :action "/" :method "GET"
                                         (:input :type "submit" :class "navbutton" :value "Next")
                                         (:input :type "hidden" :name "query" :value query)
                                         (:input :type "hidden" :name "start" :type "hidden" :value end)
                                         (:input :type "hidden" :name "end" :type "hidden" :value (+ 10 end)))))
                             (:span :id "positionticker" (fmt "displaying ~a to ~a of ~a" start end-to-display totalresultcount))
                             (:span :id "position_right_ticker" (str query-millis-phrase)))))))))))

(define-easy-handler (search-handler :uri (controller:compute-url-relative-to-root "/")
                                     :default-request-type :get)
    ((query :parameter-type 'string)
     (start :parameter-type 'integer)
     (end :parameter-type 'integer))
  (if (eql start nil)
      (progn (setf start 0)
             (setf end 10)
             (format t "hello world!")))
  (run-search-generate-page query start end))


(defvar *macceptor* (make-instance 'hunchentoot:easy-acceptor :port 8081
                                                              :document-root *install-dir*
                                                              :access-log-destination *terminal-io*
                                                              :message-log-destination *terminal-io*))

(if (hunchentoot::acceptor-shutdown-p *macceptor*)
    (hunchentoot:start *macceptor*)
    (progn
      (hunchentoot:stop *macceptor*)
      (hunchentoot:start *macceptor*)))
