(defpackage :com.vancan1ty.util
  (:nicknames :berryutils)
  (:use :common-lisp)
  (:export
   :break-transparent
   :range
   :prompt-read
   :zip-plist
   :enumerate-hash-table
   :slurp-stream4
   :simplify-line-endings
   :easy-uri-merge
   :slurp-file
   :string-is-nonempty
   :lefun
   :print-transparent
   :print-transparent-m
   :remove-prefix
   :remove-suffix))

(in-package :com.vancan1ty.util)


;;;////////////////////////////////UTILITY CODE
(defmacro break-transparent (exp)
  `(let ((x ,exp)) (break "argument to break: ~:S" x) x))

(defmacro print-transparent (exp)
  `(let ((tvar987 ,exp)) (format t "argument: ~a~%" tvar987) tvar987))

(defmacro print-transparent-m (message exp)
  `(let ((tvar987 ,exp)) (format t "p-t-m--~a: ~a~%" ,message tvar987) tvar987))


(defun range (&key (min 0) (max 0) (step 1))
  "returns range from min to max inclusive of min
   exclusive of max"
   (loop for n from min below max by step
      collect n))

(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (read-line *query-io*))

(defun zip-plist (keys values)
  "creates a plist from a list of keys and a list of values."
  (loop for k in keys
        for v in values nconc
       (list k v)))
(defun enumerate-hash-table (hasht)
  (maphash #'(lambda (k v) (format t "~a => ~a~%" k v)) hasht))

(defun slurp-stream4 (stream)
 (let ((seq (make-string (file-length stream))))
  (read-sequence seq stream)
  seq))

(defun pprint-plist (plist)
    (format t "~{~a:~10t~a~%~}~%" plist))

(defun simplify-line-endings (text)
  "replaces all sorts of weird line endings with the standard cl line ending #\newline"
  (cl-ppcre:REGEX-REPLACE-ALL "(\\r|\\n)+" text (string #\newline)))

(defun easy-uri-merge (rooturl relpath)
  "merges the relpath onto the root url, returns the result as a string"
  (with-output-to-string (ostring)
    (puri:render-uri (puri:merge-uris relpath rooturl)
		     ostring)))

(defun slurp-file (filename)
  (with-open-file (stream filename)
    (slurp-stream4 stream)))


(defun show-all-fboundp-symbols-of-package
    (package-name
     &optional (stream t))
  (let ((pack (find-package package-name)))
    (do-all-symbols (sym pack)
      (when (eql (symbol-package sym) pack)
        (when (fboundp sym)
          (format stream ":~A~%" (symbol-name sym)))))))

(defmacro time-it (&body body)
  "Measure the time it takes to execute BODY.
  Returns the result value from executing BODY and the time in seconds (floating point) as an optional second value"
  `(let* ((start-time (get-internal-real-time))
          (result ,@body)
          (end-time (get-internal-real-time))
          (elapsed-seconds (/ (- end-time start-time) internal-time-units-per-second))
          )
          (values result elapsed-seconds)))

(defun string-is-nonempty (str)
  (and str (> (length (string-trim " " str)) 0)))

(defmacro lefun (name parameters &body body)
  "Just like lefun but adds automatic logging of the parameters and the time it took to return."
  (when (typep (car body) 'string)
    (setf body (cdr body)))
  `(defun ,name ,parameters
     (format t "INFO: ~a called with parameters ~s~%" ',name (list ,@parameters))
     (let ()
       ,@body)))

(defun remove-prefix (val prefix)
  (if (string= (subseq val 0 (length prefix)) prefix)
      (subseq val (length prefix))
      val))

(defun remove-suffix (val suffix)
  (let* ((suffix-start-pos (- (length val) (length suffix))))
  (format t "~a~%" suffix-start-pos)
    (if (string= (subseq val suffix-start-pos) suffix)
        (subseq val 0 suffix-start-pos)
        val)))



;;;////////////////////////////////
