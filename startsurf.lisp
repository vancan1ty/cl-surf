(load "surf.asd")
(load "berryutils.lisp")

(asdf:load-system :surf)

(defpackage startsurf
  (:use :common-lisp :com.vancan1ty.util)
  (:import-from :uiop :ensure-pathname :ensure-directory-pathname :merge-pathnames :directory-files)
  )

(in-package :startsurf)

(defun pprint-plist (plist)
    (format t "~{~a:~10t~a~%~}~%" plist))

(defun startsurf ()
  (let* ((currentsettings controller::*currentsite*)
	 (numinindex
	   (length
            (uiop:directory-files (ensure-directory-pathname (getf currentsettings :directory))))))
    (format t "Here are your current settings~%")
    (pprint-plist currentsettings)
    (format t "there are ~a files in ~a~%" numinindex (getf currentsettings :directory))
    (format t "What would you like to do?~%
             (0) restore from disk~%
             (1) index this site again~%
             (2) index a different site~%")
    (let ((choice (parse-integer (prompt-read ""))))
      (cond ((eql choice 0)
	     (format t "restoring search for ~a~%" (getf currentsettings :siteroot))
	     (com.vancan1ty.controller::restore-search))
	    ((eql choice 1)
	     (format t "reindexing ~a~%" (getf currentsettings :siteroot))
	     (com.vancan1ty.controller::setup-search-wrapper))
	    ((eql choice 2)
	     (format t "to scan a different site, open controller.lisp and follow the instructions there.~%"))
	    (t
	     (format t "unsupported option!  try again~%")
	     (startsurf))))))

;;AUTOMATICALLY START THINGS UP...
(startsurf)
